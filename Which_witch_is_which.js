function friendDetector(list) {
    for(let i = 0; i < list.length; i++){
        if(list[i].length !== 4){
            list.splice(i, 1);
            i--;
        }
    }
    return list;
}
 
let entry1 = ["Ryan", "Kieran", "Jason", "Yous"];
let entry2 = ["George", "Nick", "Tom", "Kate", "Annie"];
let entry3 = ["James", "Will", "Jack", "Nate", "Edward"];


console.log(friendDetector(entry1))
console.log(friendDetector(entry2))
console.log(friendDetector(entry3))


function smallestSum(numbers){
    numbers.sort((a, b) => a - b);
    return numbers[0] + numbers[1];
}

let numbers1 = [5, 8, 12, 19, 22];
let numbers2 = [52, 76, 14, 12, 4];
let numbers3 = [3, 87, 45, 12, 7];

console.log(smallestSum(numbers1))
console.log(smallestSum(numbers2))
console.log(smallestSum(numbers3))